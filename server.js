'use strict';

require('dotenv').load();
if (process.env.NODE_ENV === 'production') {
    require('newrelic');
}
var app = require('./config/lib/app');
var server = app.start();
