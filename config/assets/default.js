'use strict';

module.exports = {
    client: {
        lib: {
            css: [
                'public/lib/angular/angular-csp.css',
                "public/lib/bootstrap/dist/css/bootstrap.min.css",
                'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                "public/lib/Bootflat/bootflat/css/bootflat.min.css",
                'public/lib/angular-bootstrap/ui-bootstrap-csp.css',
                'public/lib/font-awesome/css/font-awesome.min.css',
                'public/lib/ng-mobile-menu/dist/ng-mobile-menu.min.css',
                'public/lib/animate.css/animate.min.css',
                'public/lib/open-sans-fontface/open-sans.css'
            ],
            js: [
                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/angular/angular.js',
                'public/lib/bootstrap/dist/js/bootstrap.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-messages/angular-messages.js',
                'public/lib/angular-cookies/angular-cookies.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/angular-local-storage/dist/angular-local-storage.min.js',
                'public/lib/angular-translate/angular-translate.min.js',
                'public/lib/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
                'public/lib/angular-translate-storage-local/angular-translate-storage-local.js',
                'public/lib/angular-bootstrap/ui-bootstrap.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                "public/lib/angular-http-auth/src/http-auth-interceptor.js",
                'public/lib/ng-mobile-menu/dist/ng-mobile-menu.min.js',
                'public/lib/jquery-countTo/jquery.countTo.js',
                'public/lib/ng-infinite-scroll-npm-is-better-than-bower/build/ng-infinite-scroll.min.js'
            ],
            tests: ['public/lib/angular-mocks/angular-mocks.js']
        },
        css: [
            "public/css/main.css",
            'modules/*/client/css/*.css'
        ],
        less: [
            'public/less/main.less',
            'modules/*/client/less/main.less'
        ],
        lessWatch: [
            'modules/*/client/less/*.less'
        ],
        js: [
            'modules/core/client/app/config.js',
            'modules/core/client/app/init.js',
            'modules/*/client/*.js',
            'modules/*/client/**/*.js',
            "http://api.map.baidu.com/api?v=2.0&ak=olYx7rdeo03neDKMiWQaxoio"
        ],

        views: ['modules/*/client/views/**/*.html'],
        templates: ['build/templates.js']
    },
    server: {
        gruntConfig: 'gruntfile.js',
        allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
        models: 'modules/*/server/models/**/*.js',
        routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
        sockets: 'modules/*/server/sockets/**/*.js',
        policies: 'modules/*/server/policies/*.js',
        views: 'modules/*/server/views/*.html'
    }
};
