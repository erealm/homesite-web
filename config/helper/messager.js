/**
 * Created by leepc on 9/30/2016.
 */
 var path = require('path'),
    config = require(path.resolve('./config/config')),
    mongoose = require('mongoose'),
    Message  =mongoose.model('message');

exports.saveMessage = function(message) {
    if (config.mock) {
        return false;
    }
    (new Message(message)).save(function (err) {
    });
};