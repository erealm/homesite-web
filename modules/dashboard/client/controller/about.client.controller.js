/**
 * Created by lee on 9/19/2016.
 */
(function(){
    angular.module('erealm')
        .controller('aboutController',aboutController);
    aboutController.$inject=['$scope','$rootScope','DashboardService','$translate', '$interval','$http','$timeout'];
    function aboutController($scope,$rootScope, DashboardService,$translate,$interval,$http,$timeout){
        angular.extend($scope,{subTitle: "ABOUT", mainTitle: "working with you", currentPage: "about-page"});
        $scope.isCollapsed = true;
        // $scope.homeBack = 'back' + (Math.floor(Math.random() * 3) + 1);

        $scope.loadData = function(language) {
            if (!language){
                language = $translate.use();
            }
            $scope.loadLanguage = language;
            DashboardService.getEmployeeInfo(language).then(function(response){
                $scope.person = response.data;
            });
            DashboardService.getPartnersInfo(language).then(function(response){
                $scope.partner = response.data;
            });
         };
        $scope.loadData();
        DashboardService.getFlickrPhotos().then(function(response){
            var photos = response.data.items;
            angular.forEach(photos, function(item) {
                item.media.m = item.media.m.replace('_m', '_q');
            });
            $scope.photos = photos;
        });

        $scope.collapsed=function(ite)
        {
            $scope.isCollapsed=!( $scope.isCollapsed);
            $scope.item=ite;
        };

        $scope.addcount = function(){
            alert('hello erealm');
        }
        //打开页面数字不断增加
        $scope.teamConstruct = [
            {current:0,final:55},
            {current:0,final:33},
            {current:0,final:5},
            {current:0,final:15500}
        ];
        $scope.dataRise = function(arr){
            $scope.timerFirst = $interval(function(){
                arr[0].current<arr[0].final ? arr[0].current++ : $interval.cancel($scope.timerFirst);
            },19);
            $scope.timerSecond = $interval(function(){
                (arr[1].current<arr[1].final) ? arr[1].current++ : $interval.cancel($scope.timerSecond);

            },30);
            $scope.timerThird = $interval(function(){
                (arr[2].current<arr[2].final) ? arr[2].current++ : $interval.cancel($scope.timerThird);

            },200);
            $scope.timerFouth = $interval(function(){
                (arr[3].current<arr[3].final) ? arr[3].current+=60 : $interval.cancel($scope.timerFouth);
            },1);
        };
        $scope.dataRise($scope.teamConstruct);

        //瀑布流照片无限滚动
        DashboardService.getShowPhotos().then(function(response){
            $scope.imageList = response.data;
            $scope.allPhotos = true;
            $scope.imageListLife = [];
            $scope.lifePhotos = false;
            $scope.imageListWork = [];
            $scope.workPhotos = false;
            $scope.imageListCharity = [];
            $scope.charityPhotos = false;
            angular.forEach($scope.imageList,function(value){
                if(value.tag == "生活")
                    $scope.imageListLife.push(value);
                else if(value.tag == "工作")
                    $scope.imageListWork.push(value);
                else if(value.tag == "慈善")
                    $scope.imageListCharity.push(value);
            });

            $scope.images = $scope.imageList.slice(0,4);
            $scope.imagesLife = $scope.imageListLife.slice(0,4);
            $scope.imagesWork = $scope.imageListWork.slice(0,4);
            $scope.imagesCharity = $scope.imageListCharity.slice(0,4);

            $scope.imagesInfo = [
                {heading: 'all', template: 'all_template', images: $scope.images},
                {heading: 'life', template: 'life_template', images: $scope.imagesLife},
                {heading: 'work', template: 'work_template', images: $scope.imagesWork},
                {heading: 'charity', template: 'charity_template', images: $scope.imagesCharity}
            ];

            $scope.allPhotosSelect = function(index){
                console.log(index);
                console.log($scope.imagesInfo[index]);
                $scope.currentImages = $scope.imagesInfo[index].images;

                // console.log($event.target);
                // $timeout(function(){
                //     $scope.allPhotos = true;
                //     $scope.lifePhotos = false;
                //     $scope.workPhotos = false;
                //     $scope.charityPhotos = false;
                // },500);
            };
            $scope.lifePhotosSelect = function(){
                // $timeout(function(){
                //     $scope.allPhotos = false;
                //     $scope.lifePhotos = true;
                //     $scope.workPhotos = false;
                //     $scope.charityPhotos = false;
                // },500);

            };
            $scope.workPhotosSelect = function(){
                // $timeout(function(){
                //     $scope.allPhotos = false;
                //     $scope.lifePhotos = false;
                //     $scope.workPhotos = true;
                //     $scope.charityPhotos = false;
                // },500);

            };
            $scope.charityPhotosSelect = function(){
                // $timeout(function(){
                //     $scope.allPhotos = false;
                //     $scope.lifePhotos = false;
                //     $scope.workPhotos = false;
                //     $scope.charityPhotos = true;
                // },500);
            };


            $scope.loadSize = 1;
            $scope.loadFinish = false;
            $scope.loadMore = function(){
                    ($scope.images.length >= $scope.imageList.length) ? ($scope.loadFinish = true) :  $scope.images = $scope.imageList.slice(0,$scope.images.length+$scope.loadSize);
                    ($scope.imagesLife.length >= $scope.imageListLife.length) ? ($scope.loadFinish = true) :  $scope.imagesLife = $scope.imageListLife.slice(0,$scope.imagesLife.length+$scope.loadSize);
                    ($scope.imagesWork.length >= $scope.imageListWork.length) ? ($scope.loadFinish = true) :  $scope.imagesWork = $scope.imageListWork.slice(0,$scope.imagesWork.length+$scope.loadSize);
                    ($scope.imagesCharity.length >= $scope.imageListCharity.length) ? ($scope.loadFinish = true) :  $scope.imagesCharity = $scope.imageListCharity.slice(0,$scope.imagesCharity.length+$scope.loadSize);
            };
        });
    }
}());