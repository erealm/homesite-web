/**
 * Created by pc11 on 10/31/2016.
 */
(function(){
    'use strict';
    angular.module('erealm').controller('blogDetailController',blogDetailController);
    blogDetailController.$inject = ['$scope','DashboardService','$translate','$stateParams'];
    function blogDetailController($scope,DashboardService,$translate,$stateParam){
        // angular.extend($scope, {subTitle: "Hello, We are", mainTitle: "eRealm Info & Tech", currentPage: "blog-page"});
        $scope.loadData = function (language) {
            if (!language) {
                language = $translate.use();
            }
        };
        $scope.loadData();
        console.log($stateParam.tag);
    }
}());