/**
 * Created by leepc on 9/19/2016.
 */
(function(){
    angular.module('erealm')
        .controller('contactController',contactController);
    
    
    contactController.$inject = ['$scope', 'DashboardService', '$translate'];
    function contactController($scope, DashboardService, $translate) {
        'use strict';
        angular.extend($scope,{subTitle: "CONTACT", mainTitle: "Say Hello", currentPage: "contact-page"});

        //var google = window.google;

        $scope.loadData = function(language) {
            if (!language){
                language = $translate.use();
            }

            DashboardService.getContactsInfo(language).then(function(response){
                $scope.contacts = response.data;
                // console.log($scope.contacts);
                // $scope.changeMap(response.data[0]);
            });
        };
        $scope.loadData();

        $scope.changeMap = function(contactInfo) {

            $scope.activeMap = contactInfo.name;
            console.log( $scope.activeMap);

            var BMap = window.BMap;
            var latLng = contactInfo.map.latLng;
            var location = contactInfo.map.point;
            var map = new BMap.Map("google_map");
            var point = new BMap.Point(latLng[1],latLng[0]);
            var center = new BMap.Point(location[1],location[0]);
            map.addControl(new BMap.NavigationControl());
            map.addControl(new BMap.ScaleControl());
            map.addControl(new BMap.OverviewMapControl());
            map.addControl(new BMap.MapTypeControl());
            map.disable3DBuilding();
            map.setMapStyle({style:'grayscale'});
            map.centerAndZoom(point, 15);


            new BMap.Icon('/img/icon_marker.png', new BMap.Size(300,157));
            var marker = new BMap.Marker(center);
            map.addOverlay(marker);
            var opts = {
                width : 200,
                height: 100,
                title : "eRealm Info & Tech",
                enableMessage:false
            };

            var address = contactInfo.info.address + ' ' + contactInfo.info.region + ' ' + contactInfo.info.county;

            var infoWindow = new BMap.InfoWindow(address, opts);
            marker.addEventListener("click", function(){
                map.openInfoWindow(infoWindow, center);
            });
        };
        $scope.sendMessage = function() {
            if(!$scope.contact_form.$invalid){
                $scope.loading = true;
                DashboardService.submitMessage($scope.name, $scope.email, $scope.message).then(function(){
                });
            }
        };
        
        $scope.reloading = function(){
            $scope.loading = false;
            $scope.contact_form.$setPristine();
        };
    }
}());
