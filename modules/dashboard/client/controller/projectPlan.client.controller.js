/**
 * Created by jaydon on 9/27/2016.
 */
(function(){
    'use strict';
    angular.module('erealm')
        .controller('projectPlanController',projectPlanController);

    projectPlanController.$inject = ['$scope' , 'DashboardService' , '$translate'];
    function projectPlanController($scope,DashboardService,$translate){
        angular.extend($scope,{subTitle: "PROJECT", mainTitle: "submit your project", currentPage: "plan-page"});
        // console.log($scope.project_form);
        $scope.sendMessage = function(){
            if($scope.project_form.$valid){
                console.log('-------------------');
                DashboardService.submitProjectPlan($scope.name, $scope.email, $scope.company,$scope.message,$scope.type,$scope.telephone,$scope.budget).then(function(){
                    $scope.errorMessage = $translate('sent_successfully');
                }, function(){
                   
                    $scope.errorMessage = $translate('sent_fault');
                });
            }
        };

        $scope.reloading = function(){
            // $scope.loading = false;
            $scope.project_form.$setPristine();
            // $scope.errorMessage = $translate('make_sure');
        };
        var invalid_cn = {
            'required':'不能为空',
            'name': '用户名不合法',
            'email': '请输入正确的电子邮箱地址',
            'company': '公司名称不能为空',
            'telephone': '联系电话有误',
            'message': '输入信息不能为空'
        };
        var invalid_en = {
            'required':'This is required ',
            'name': 'name is not valid ',
            'email': 'Please enter a correct e-mail address ',
            'company': 'Please enter a correct company name ',
            'telephone': 'Please enter a correct telephone number ',
            'message': 'Please enter some of your information '
        };
        var types_en =  [
            {name:'Website'},
            {name:'Mobile'},
            {name:'Application'  },
            {name:'Other'}
        ];

        var budgets_en = [
            {"name" : "< $50,000"},
            {"name" : "$50,000 - $100,000"},
            {"name" : "$100,000 - $150,000"},
            {"name" : "$150,000 - $200,000"},
            {"name" : "> $200,000"}
        ];
        var types_cn =  [
            {name:'网站'},
            {name:'移动应用'},
            {name:'应用程序'  },
            {name:'其它'}
        ];
        var budgets_cn = [
            {"name" : "< ￥50,000"},
            {"name" : "￥50,000 - ￥100,000"},
            {"name" : "￥100,000 - ￥150,000"},
            {"name" : "￥150,000 - ￥200,000"},
            {"name" : "> ￥200,000"}
        ];
        $scope.loadData = function(language) {
            if (!language){
                language = $translate.use();
            }
            $scope.errorMessage = $translate('make_sure');
            if (language === 'en') {
                $scope.types =  types_en;
                $scope.budgets =  budgets_en;
                $scope.eMessage = invalid_en;

            } else {
                $scope.types = types_cn;
                $scope.budgets =  budgets_cn; 
                $scope.eMessage = invalid_cn;
            }
        };
        $scope.loadData();

    }

}());