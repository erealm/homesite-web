/**
 * Created by leepc on 9/19/2016.
 */


(function () {
    'use strict';
    
    angular
        .module('erealm.service')
        .service('DashboardService', DashboardService);

    DashboardService.$inject = ['$http'];

    function DashboardService ($http) {
        this.getContactsInfo = function (language) {
            return $http.get('/app/contact/'+language);
        },
        this.submitMessage = function(name, email, message){
            return $http.post('/app/message', {name: name, email:email, message: message});
        },
        this.getManagement = function(language){
            return $http.get('app/management/'+language);
        },
        this.getTechnologies = function(){
            return  $http.get('/app/technology');
             }
            // this.checkPosts = function(language){
            //     return $http.get('/back/postscheck/' + language);
            // },
        this.getEmployeeInfo = function(language){
            console.log(1);
            return $http.get('/app/personnel/'+language);
        },
        this.getPartnersInfo = function(language){
            return  $http.get('/app/partners/'+language);
        },
        this.getFlickrPhotos = function(){
           return $http.get('/app/teamphotos');
        },
        this.submitProjectPlan = function(name, email,company,message,type,telephone,budget){
                return $http.post('/app/projectplan', {name: name, email:email, message: message,company:company,type:type,telephone:telephone,budget:budget});
        },
            this.getShowPhotos = function(){
                return $http.get('/app/showPhotos');
        }
        
    }

}());
