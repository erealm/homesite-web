/**
 * Created by leepc on 9/19/2016.
 */
(function(){
    'use strict';
    angular.module('erealm.routes')
        .config(routeConfig);
    routeConfig.$inject=['$stateProvider'];
    function routeConfig($stateProvider){
        $stateProvider.
            state('home',{
            url:'/',
            templateUrl:'modules/dashboard/client/views/home.client.view.html',
            controller:'homeController',
            data: {
                pageTitle: 'Home'
            }
        })
            .state('about',{
                url:'/about',
                templateUrl:'modules/dashboard/client/views/about.client.view.html',
                controller:'aboutController',
                data: {
                    pageTitle: 'About'
                }
            })
            .state('news',{
                url:'/news',
                templateUrl:'modules/dashboard/client/views/blog.client.view.html',
                controller:'newsController',
                data: {
                    pageTitle: 'News'
                }
            })
            .state('contact',{
                url:'/contact',
                templateUrl:'modules/dashboard/client/views/contact.client.view.html',
                controller:'contactController',
                data: {
                    pageTitle: 'Contact'
                }
            })
            .state('projectplan',{
                url:'/projectplan',
                templateUrl:'modules/dashboard/client/views/projectPlan.client.view.html',
                controller:'projectPlanController',
                data: {
                    pageTitle: 'projectPlan'
                }
            })
            .state('blogDetail',{
                params:{"tag":null},
                url:'/blogDetail',
                templateUrl:'modules/dashboard/client/views/blogDetail.client.view.html',
                controller: 'blogDetailController',
                data: {
                    pageTitle: 'blogDetail'
                }
            })
    }
}());