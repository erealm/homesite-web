/**
 * Created by jaydon lee on 9/26/2016.
 */
(function(){
    'use strict';
    var erealm = angular.module('erealm');
    erealm.directive('language',chooselanguage);
    chooselanguage.$inject = ['$translate'];
    function chooselanguage ($translate){
        return {
            scope: true,
            restrict:'EA',
            loadData:'=loadData',
            link: function(scope,element,attrs){
                angular.element(element).bind('click',function(){
                    $translate.use(attrs.key);
                    if(scope.loadData){
                        scope.loadData(attrs.key);
                    }
                    scope.$apply();
                })
            }
        }
    }

    erealm.directive('navigation',showNavMenu);
    showNavMenu.$inject = [];
    function showNavMenu (){
        return {
            restrict:'E',
            template:
            '<div>'
            + '<div class="muti-language">'


            + '<div class="btn-group clearfix" uib-dropdown on-toggle="toggled(open)">'
            + '<button type="button" class="btn btn-default" translate="language"></button>'
            + '<button type="button" class="btn btn-default dropdown-toggle" uib-dropdown-toggle>'
            + '<span class="caret"></span>'
            + '<span class="sr-only">Toggle Dropdown</span>'
            + '</button>'
            + '<ul class="dropdown-menu" role="menu" uib-dropdown-menu>'
            + '<li role="menuitem"><a  href="javascript:void(0)" language key="cn">简体中文</a></li>'
            + '<li class="divider"></li>'
            + '<li role="menuitem"><a  href="javascript:void(0)" language key="en">English</a></li>'
            + '</ul>'
            + '</div>'

            + '</div>'
            + '<div class="nav-container">'
            + '<nav ng-class="{fixed: showFixedHeader}" >'
            + '<div class="wrap">'
            + '<div class="sub-logo-mark"><a href="/"></a></div>'
            + '<div class="nav-link fa fa-bars"  ng-click="$spMenu.toggle()"></div>'
            + '<ul id="navslide">'
            + "<li class=\"withripple\" ng-class=\"{active: currentPage == 'home-page'}\"><a href=\"/\" translate=\"home\">home</a></li>"
            + "<li class=\"withripple\" ng-class=\"{active: currentPage == 'blog-page'}\"><a href=\"/news\" translate=\"blog\">news</a><span class=\"blog-badge ng-hide\" ng-bind=\"postsNotification\" ng-show=\"postsNotification > 0\">1</span></li>"
            + "<li class=\"withripple\" ng-class=\"{active: currentPage == 'contact-page'}\"><a href=\"/contact\" translate=\"contact\">contact</a></li>"
            + "<li class=\"withripple\" ng-class=\"{active: currentPage == 'about-page'}\"><a href=\"/about\" translate=\"about\">about</a></li>"
            + '</ul>'
            + '</div>'
            + '</nav>'
            + '</div>'
            + '<div id="sp-nav" class="page-menu-slide">'
            + '<ul class="nav-slide">'
            + '<li> <a href="/" class="fa fa-home"><span translate="home" ></span></a></li>'
            + '<li><a href="/news" class="fa fa-newspaper-o"><span translate="blog"></span></a></li>'
            + '<li><a href="/contact"  class="fa fa-comments-o"><span translate="contact" ></span></a></li>'
            + '<li><a href="/about"  class="fa fa-group"><span translate="about" ></span></a></li>'
            // + '<li class="divider"></li>'
            + '</ul>'
            + '<button class="btn common-btn btn_choose_language" ><a  href="javascript:void(0)" language key="cn">简体中文</a></button>'
            + '<button class="btn common-btn btn_choose_language"><a  href="javascript:void(0)" language key="en">English</a></button>'
            + '</div>'
            + '<div ng-transclude ng-click="$spMenu.hide()"></div>'
            + '</div>',
            transclude: true
        }
    }

    //window scroll
    erealm.directive('scroll',doScroll);
    doScroll.$inject = ['$window']; 
    function doScroll ($window){
        return function(scope){
            angular.element($window).bind('scroll',function(){
                    scope.showFixedHeader = ($window.document.body.scrollTop > 300);//280
                scope.$apply();
            })

        }
    }
    //home-page progressbar
    erealm.directive('aboutScroll',aboutScroll);
    aboutScroll.$inject = [ '$window', '$interval' ];
    function aboutScroll($window,$interval) {
        return function(scope){
            angular.element($window).bind('scroll',function(){
                if($window.document.body.scrollTop >300 && $window.document.body.scrollTop <600){
                    angular.forEach(scope.aboutprogress,function(data){
                        var standard = data.stop;
                        data.value = 0;
                        var stop = $interval(function(){
                            if(data.value < standard){
                                data.value+=1;
                            }
                        },100);
                    });
                }
                scope.$apply();
            })
        }
    }
    //block rise
    erealm.directive('blockRise',blockRise);
    blockRise.$inject = [ '$window', '$interval' ];
    function blockRise($window,$interval) {
        return function(scope){
            angular.element($window).bind('scroll',function(){
                ($window.document.body.scrollTop > 300)&&(scope.isRise = true);
                ($window.document.body.scrollTop > 1120)&& (scope.isRiseCan = true);
                ($window.document.body.scrollTop > 1600)&&(scope.isRiseMan = true);
                ($window.document.body.scrollTop > 1800)&&(scope.isSlideLeftFirst = true);
                ($window.document.body.scrollTop > 2100)&&(scope.isSlideRightFirst = true);
                ($window.document.body.scrollTop > 2400)&&(scope.isSlideLeftSecond = true);
                scope.$apply();
            })
        }
    }
    //back to top
    erealm.directive("backtop",  ['$window',function () {
        return function(scope, element) {
            angular.element(element).bind("click", function() {
                $('body,html').animate({
                    scrollTop: 0
                }, 500);
                scope.$apply();    
                return false;
            });
        };
    }])
}());