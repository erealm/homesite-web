'use strict';

module.exports = function (app) {

    var dashboard = require('../controllers/dashboard.server.controller.js');

    app.route('/app/contact/:language').get(dashboard.readContacts);
    app.route('/app/message').post(dashboard.sendMessage);
    app.route('/app/management/:language').get(dashboard.getTeamManagement);
    app.route('/app/technology').get(dashboard.getTeamTech);
    // app.route('/back/postscheck/:language').get(dashboard.checkPosts);
    app.route('/app/personnel/:language').get(dashboard.readStaff);
    app.route('/app/partners/:language').get(dashboard.readPartner);
    app.route('/app/teamphotos').get(dashboard.getTeamPhotos);
    app.route('/app/projectplan/:language').post(dashboard.sendProject);
    app.route('/app/showPhotos').get(dashboard.getShowPhotos);
};
