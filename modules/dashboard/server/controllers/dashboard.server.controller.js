'use strict';
var validate = require('validator');
var mongoose = require('mongoose');
var path = require('path');
var responseHandler = require(path.resolve('./config/helper/responseHelper'));
var config = require(path.resolve('./config/config'));
var mailer = require(path.resolve('./config/helper/mailer'));
var Partner = mongoose.model('partner');
var Personnel = mongoose.model('person');
exports.readContacts = function(req, res) {
    var language = req.params.language;
    res.send(require(path.resolve('./public/mock/contact_' + language +'.json')));

};

exports.getTeamManagement = function(req, res) {
    var language = req.params.language;
    res.send(require(path.resolve('./public/mock/management_' + language +'.json')));

};

exports.getTeamTech = function(req, res) {

    res.send(require(path.resolve('./public/mock/technology.json')));
};
// exports.checkPosts = function(req, res) {
//     var language = req.params.language;
//     var currentId = util.parseCookies(req).BLOGS_CHECK;
//
//     if (config.mock) {
//         var current = require(path.resolve('./public/mock/blog_' + language +'.json'))[0];
//         res.send({result: current.id == currentId? 0:1});
//     } else {
//         client.posts("erealm", {tag: language}, function(err, resp) {
//             var count = Math.min(resp.posts.length, 5);
//             if (currentId) {
//                 for(var i= 0,len = resp.posts.length;i<len;i++) {
//                     if (currentId == resp.posts[i].id) {
//                         count = i;
//                         break;
//                     }
//                 }
//             }
//
//             res.send({result: count});
//         });
//     }
// };
exports.readStaff = function(req, res) {
    var language = req.params.language;
    // res.send(require(path.resolve('./public/mock/personnel_' + language +'.json')));
    Personnel.find({}).exec(function(err,person){
        if (err) {
            return res.status(401).send();
        } else {
            return res.send(person);
        }
    })
};
exports.readPartner = function(req, res) {
    var language = req.params.language;
    Partner.find({}).exec(function (err, partners) {
        if (err) {
            return res.status(401).send();
        } else {
            // console.log('-------------------------------');
            // console.log(partners);
            return res.send(partners);
        }
    });
};
exports.getTeamPhotos = function(req, res) {
    res.send(require(path.resolve('./public/mock/flickr.json')));
};
exports.getShowPhotos = function(req, res) {
    res.send(require(path.resolve('./public/mock/waterfall.json')));
};

//sendProjectPlan
function parseCookies(request) {
    var list = {},
        rc = request.headers.cookie;

    rc.split(';').forEach(function(cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = unescape(parts.join('='));
    });
    return list;
}
//
var cn = require(path.resolve('./public/mock/content_cn.json'));
var en = require(path.resolve('./public/mock/content_en.json'));

exports.sendProject = function(req, res) {
    var   name = req.body.name;
    var   email = req.body.email;
    var   message = req.body.message;
    var   company = req.body.company;
    var   type = req.body.type;
    var   telephone = req.body.telephone;
    var   budget = req.body.budget;

    req.checkBody('name', 'Invalid postparam').notEmpty().len(0, 20);
    req.checkBody('email', 'Invalid postparam').notEmpty().isEmail().len(0, 50);
    req.checkBody('message', 'Invalid postparam').notEmpty().len(0, 300);
    req.checkBody('company', 'Invalid postparam').notEmpty().len(0, 30);
    req.checkBody('type', 'Invalid postparam').notEmpty().len(0, 20);
    req.checkBody('telephone', 'Invalid postparam').notEmpty().len(0, 14);
    req.checkBody('budget', 'Invalid postparam').notEmpty().len(0, 20);

    var errors = req.validationErrors();
    if (errors) {
        res.send('There have been validation errors', 400);
        return;
    }

    var storagekey = parseCookies(req).NG_TRANSLATE_LANG_KEY;
    var language = '';
    if (storagekey === '"cn"') {
        language = cn;
    } else {
        language = en;
    }
    mailer.sendTemplate(name + '<' + email + '>', 'messagePlanReciced', {
        language: language,
        fullName: name
    });
    mailer.sendTemplate(config.support, 'newProjectplanMessage', {
        language: language,
        name: name,
        email: email,
        message: message,
        company: company,
        type: type,
        telephone: telephone,
        budget: budget
    });
    console.log(storagekey);

    //save the email message into mongodb
    require('../helper/saveEmail').saveMessage({
        language: language,
        name: name,
        email: email,
        message: message,
        company: company,
        type: type,
        telephone: telephone,
        budget: budget
    });

    res.json({
        code: 200,
        success: true
    });
};
exports.sendMessage = function(req, res){
    var name = req.body.name,
        email = req.body.email,
        message = req.body.message;
    req.checkBody('name', 'Invalid postparam').notEmpty().len(0, 20);
    req.checkBody('email', 'Invalid postparam').notEmpty().isEmail().len(0, 50);
    req.checkBody('message', 'Invalid postparam').notEmpty().len(0, 300);
    var errors = req.validationErrors();
    if (errors) {
        res.send({code: 'error_005'});
        return;
    }

    var storagekey = util.parseCookies(req).NG_TRANSLATE_LANG_KEY;
    console.log(storagekey);
    var language = '';
    if (storagekey === '"cn"') {
        language = cn;
    } else {
        language = en;
    }
    mailer.sendTemplate(name + '<' + email + '>', 'messageReciced', {
        language: language,
        fullName: name
    });
    mailer.sendTemplate(config.support, 'newContactMessage', {
        language: language,
        name: name,
        email: email,
        message: message
    });

    //save the email message into mongodb
    // require('../helper/messager').saveMessage({
    //     name: name,
    //     email: email,
    //     message: message
    // });

    res.json({code: 'success_001'});
};

