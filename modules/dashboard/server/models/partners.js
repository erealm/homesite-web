/**
 * Created by root on 11/14/2014.
 */
'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var PartnerSchema = new Schema({
    number: { type: String, default: '' },
    photo: { type: String, default: '' },
    first_name_en: { type: String, default: '' },
    first_name_cn: { type: String, default: '' },
    last_name_en: { type: String, default: '' },
    last_name_cn: { type: String, default: '' },
    position_en: { type: String, default: '' },
    position_cn: { type: String, default: '' },
    hobby_en: { type: String, default: '' },
    hobby_cn: { type: String, default: '' },
    description_en: { type: String, default: '' },
    description_cn: { type: String, default: '' },
    motto_en: { type: String, default: '' },
    motto_cn: { type: String, default: '' }
});

module.exports = mongoose.model('partner', PartnerSchema,'partners');
