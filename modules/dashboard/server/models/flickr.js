/**
 * Created by leepc on 9/30/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var FlickrSchema = new Schema({
    title: { type: String, default: '' },
    link: { type: String, default: '' },
    description: { type: String, default: '' },
    modified:{ type: String, default: ''},
    generator:{ type: String, default: ''},
    items:{ type: Array}
});
module.exports = mongoose.model('flickr', FlickrSchema,'flickr');