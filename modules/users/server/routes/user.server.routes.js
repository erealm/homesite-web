'use strict';

var path = require('path'),
    loginHelper = require(path.resolve('./config/helper/loginHelper'));

module.exports = function (app) {

    app.route('/app/login').post(loginHelper.login);

};
