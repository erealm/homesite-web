(function () {
    'use strict';

    // Authentication service for user variables

    angular
    .module('users.services')
    .factory('Authentication', Authentication);

    Authentication.$inject = ['$q', 'localStorageService', '$injector', 'authService'];
    function Authentication($q, localStorageService, $injector, authService) {

        var _authenticationUser = localStorageService.get('authorizationData') || {};

        var _logOut = function () {
            localStorageService.remove('authorizationData');
            Object.keys(_authenticationUser).forEach(function (key) {
                delete _authenticationUser[key];
            });
        };

        var _login = function (loginData) {
            var $http = $injector.get('$http');

            var data = {
                grant_type: 'password',
                username: loginData.username,
                password: loginData.password,
                rememberMe: !!loginData.isRememberMe
            };
            var deferred = $q.defer();

            $http.post('/app/login', data).success(function (response) {
                console.log(response);
                if (response.access_token && response.refresh_token) {
                    _authenticationUser.token = response.access_token;
                    _authenticationUser.refreshToken = response.refresh_token;
                    _authenticationUser.user_id = response.user_id;
                    _authenticationUser.expires_in = response.expires_in;
                    _authenticationUser.token_type = response.token_type;

                    localStorageService.set('authorizationData', _authenticationUser);

                    $http.get('/controllers/base/users/' + _authenticationUser.user_id).success(function (profile) {
                        if (!profile.profileImageURL) {
                            profile.profileImageURL = "img/default-profile-image.png";
                        }
                        angular.merge(_authenticationUser, profile);
                        localStorageService.set('authorizationData', _authenticationUser);
                        _authenticationUser.isAuth = true;

                        deferred.resolve(_authenticationUser);
                    }).error(function (err, status) {
                        _logOut();
                        deferred.reject(err);
                    });
                } else {
                    _logOut();
                    return deferred.reject('Not Authenticated!');
                }

            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _getUserProfile = function (user_id) {
            var $http = $injector.get('$http');
            var deferred = $q.defer();
            return $http.get('/controllers/base/users/' + user_id).success(function (results) {
                deferred.resolve(results);
            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });
            return deferred.promise;
        };

        var _fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authenticationUser.isAuth = true;
                _authenticationUser.username = authData.username;
            }

        };

        var _refreshToken = function () {
            var deferred = $q.defer();
            var $http = $injector.get('$http');

            var authData = localStorageService.get('authorizationData');

            if (authData) {
                var data = {
                    grant_type: 'refresh_token',
                    refresh_token: authData.refreshToken,
                    client_id: authSettings.clientId,
                    client_secret: authSettings.clientSecret
                };

                localStorageService.remove('authorizationData');

                $http.post('/app/login', data).success(function (response) {
                    if (response.access_token && response.refresh_token) {
                        _authenticationUser.token = response.access_token;
                        _authenticationUser.refreshToken = response.refresh_token;
                        localStorageService.set('authorizationData', _authenticationUser);

                        deferred.resolve(response);
                    } else {
                        _logOut();
                        return deferred.reject('Not Authenticated!');
                    }


                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
            }

            return deferred.promise;
        };

        return {
            user: _authenticationUser,
            login: _login,
            logOut: _logOut,
            getUserProfile: _getUserProfile,
            fillAuthData: _fillAuthData,
            refreshToken: _refreshToken,
            loginConfirmed: authService.loginConfirmed
        };
    }
}());
