(function () {
    'use strict';

    // Authentication service for user variables

    angular
        .module('users.services')
        .service('AccountService', AccountService);

    AccountService.$inject = ['$http'];

    function AccountService ($http) {
        this.createAccount = function (user) {
            return $http.post('/controllers/base/users', user);
        },
        this.modifyAccount = function (userID, user) {
            return $http.put('/controllers/base/users/'+userID, user);
        },
        this.changePassword = function (userId, oldPassword, newPassword) {
            return $http.post('/controllers/base/users/' + userId + '/password/change', {
                password: oldPassword,
                newPassword: newPassword,
                confirmPassword: newPassword
            });
        },
        this.forgotPassword = function (user) {
            return $http.post('/controllers/base/users/password/forgot', user);
        },
        this.checkResetToken = function (token) {
            return $http.get('/controllers/base/users/password/token/' + token);
        },
        this.findPassword = function (passwordInfo) {
            return $http.patch('/controllers/base/users/password/reset', passwordInfo);
        },
        this.changePassword = function (userId, passwordInfo) {
            return $http.post('/controllers/base/users/' + userId + '/change/password', passwordInfo);
        }
    }

}());
