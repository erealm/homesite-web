(function () {
    'use strict';

    angular
        .module('users.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {

        $stateProvider
            .state('users', {
                url: '/users',
                templateUrl: 'modules/users/client/views/index.client.view.html',
                controller: 'userInfoController',
                data: {
                    pageTitle: 'Users'
                }
            })
            .state('login', {  
                url: '/login',
                templateUrl: 'modules/users/client/views/login.client.view.html',
                controller: 'loginController',
                data: {
                    pageTitle: 'SignIn'
                },
                params: {currentAction : null}
            })
            .state('findPassword', {
                url: '/reset-password/:token',
                templateUrl: 'modules/users/client/views/findPassword.client.view.html',
                controller: 'findPasswordController',
                data: {
                    pageTitle: 'Reset Password'
                }
            })
            .state('index',{
                url:'/indexsite',
                template:'moduls/users/client/views/indexsite.client.view.html',
                controller:'indexsiteController',
                data:{
                    pageTile:'IndexSite'
                }
            })
            .state('changePassword', {
                url: '/changePassword',
                templateUrl: 'modules/users/client/views/changePassword.client.view.html',
                controller: 'changePasswordController',
                data: {
                    pageTitle: 'Change Password'
                }
            });
    }
}());
