(function () {
    'use strict';

    angular
        .module('users')
        .controller('userInfoController', userInfoController)

    userInfoController.$inject = ['$rootScope', '$scope', 'Authentication', '$state', 'AccountService'];

    function userInfoController($rootScope, $scope, Authentication, $state, AccountService) {
        $scope.userInfo = Authentication.user;
    }
}());
