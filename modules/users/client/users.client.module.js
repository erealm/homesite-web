(function (app) {
    'use strict';

    app.registerModule('users', ['core']);
    app.registerModule('users.routes', ['ui.router', 'core.routes']);
    app.registerModule('users.services');
}(ApplicationConfiguration));
