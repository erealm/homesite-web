/**
 * Created by Don on 6/2/2016.
 */

(function () {
    'use strict';

    angular.module('users')
        
    .directive('match', match);
    match.$inject = [];

    function match() {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=match"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.match = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    }
}());
