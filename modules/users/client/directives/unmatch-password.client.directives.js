/**
 * Created by Don on 6/2/2016.
 */

(function () {
    'use strict';

    angular.module('users')
        
    .directive('unmatch', unmatch);
    unmatch.$inject = [];

    function unmatch() {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=unmatch"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.unmatch = function(modelValue) {
                    return modelValue !== scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    }
}());
